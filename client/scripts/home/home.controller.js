(function () {
  'use strict';
  angular
    .module('app.home',[])
      .controller('HomeCtrl',['Tour','ngNotify',function (Tour,ngNotify) {

        var vm = this;

        Tour.all()
          .success(function (data) {
              vm.tours = data.filter(function (item) {
                return item.visible == true
              });
          })
          .error(function (err) {
            ngNotify.set(err, 'error');
          });        

      }]);
})();
