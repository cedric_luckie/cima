(function () {
  'use strict';

  angular
    .module('app', [
      'ngRoute',
      'ngNotify',
      'mobile-angular-ui',
      'angular-loading-bar',
      'mobile-angular-ui.gestures',
      'app.home',
      'app.main',
      'app.tour',
      'app.user'
    ])
    .run(['ngNotify','$transform',function(ngNotify,$transform) {
      
      window.$transform = $transform;

      ngNotify.error = function (text) {
        ngNotify.set(text,'error');
      };

    }])
    .config(['$routeProvider','$locationProvider',function ($routeProvider,$locationProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'scripts/home/scroll.html',
          reloadOnSearch: false,
          controller:'HomeCtrl',
          controllerAs:'ctrl'
        })
        .when('/acerca-de',{
          templateUrl:'scripts/shared/cima.html'
        })
        .when('/contacto',{
          templateUrl:'scripts/shared/contacto.html'
        })
        .when('/tour/:id', {
          template: '<tour></tour>'
        })
        .when('/admin', {
          template: '<admin></admin>'
        })
        .when('/admin/tours', {
          template: '<list-tours></list-tours>'
        })
        .when('/admin/tours/nuevo', {
          template: '<create-tour></create-tour>'
        })
        .when('/admin/tour/:id', {
          template: '<tour-update></tour-update>'
        })
        .otherwise({
          redirectTo: '/'
        });

        $locationProvider.html5Mode(true);
    }]);

})();
