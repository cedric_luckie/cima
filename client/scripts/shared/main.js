(function () {
  'use strict';
    angular
      .module('app.main', [
      'app.directives',
      'app.filters'
    ])
    .controller('MainController',['$rootScope','User', function($rootScope, User){

      var vm = this;

      User.api()
        .success(function (data) {
          vm.api = data;
        })
        .error(function (err) {
          console.error(err)
        });

      

      // User agent displayed in home page
      // $scope.userAgent = navigator.userAgent;

      // Needed for the loading screen
      $rootScope.$on('$routeChangeStart', function(){
        $rootScope.loading = true;
      });

      $rootScope.$on('$routeChangeSuccess', function(){
        $rootScope.loading = false;
      });


    }]);
})();
