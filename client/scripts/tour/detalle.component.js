(function () {
  'use strict';
  angular
    .module('app.tour.detalle',[])
      .component('tour',{
        templateUrl:'scripts/tour/views/detalle.html',
        controller:'TourController'
      })
      .controller('TourController',['Tour','ngNotify','$routeParams',function (Tour,ngNotify,$routeParams) {

        var vm = this;

        vm.toggle = true;

        Tour.show($routeParams.id)
          .success(function (data) {
            vm.tour = data;
            vm.tour.src = './tours/' + data.id + '/index.html';
          })
          .error(ngNotify.error);
      }])
})();
