(function () {
  'use strict';
  angular
    .module('app.tour.service',  [])

      .factory ('Tour', ['$http',function ($http) {

        return {

          update:function (data) {
            return $http({
              method:'PUT',
              data:data,
              url:'/api/tours/' + data.id
            });
          },
          upload:function (fd,titulo) {
            return $http.post('/api/tours/?titulo=' + titulo, fd, {
              withCredentials: true,
              headers: {'Content-Type': undefined },
              transformRequest: angular.identity
            });
          },
          imagen:function (id,fd) {
            return $http.post('/api/tours/' + id, fd, {
              withCredentials: true,
              headers: {'Content-Type': undefined },
              transformRequest: angular.identity
            });
          },
          uploadDir:function (fd) {
            return $http.post('/api/tours/dir', fd, {
              withCredentials: true,
              headers: {'Content-Type': undefined },
              transformRequest: angular.identity
            });
          },
          subscribe:function (data) {
            return $http({
              method:'POST',
              data:data,
              url:'/api/users/subscribe'
            });
          },
          show:function (id) {
            return $http.get('/api/tours/' + id);
          },
          delete:function (id) {
            return $http.delete('/api/tours/' + id);
          },
          all:function () {
            return $http.get('api/tours');
          },
          toggleTour:function (id) {
            return $http.patch('api/tours/' + id);
          }
        };
      }]);

})();
