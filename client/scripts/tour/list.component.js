(function () {
  'use strict';
  angular
    .module('app.tour.list',[])
      .component('listTours',{
            templateUrl: 'scripts/tour/list.html',
            controller:'ListToursCtrl',
            controllerAs:'ctrl'
      })
      .controller('ListToursCtrl',['ngNotify','Tour', function(ngNotify,Tour){

        var vm = this;

        function getTours() {
          Tour.all()
            .success(function (data) {
              vm.tours = data;
            })
            .error(function(err) {
              ngNotify.set(err,'error');
            });
        }

        getTours();

        vm.delete = function (id) {
          var si = confirm('¿Realmente deseas eliminar este tour?');
          if (si) {
            Tour.delete(id)
            .success(function (data) {
              ngNotify.set(data,'success');
              getTours();
            })
            .error(function (err) {
              ngNotify.set(err,'error')
            });
          }
        };

        vm.toggleTour = function (id) {
          Tour.toggleTour(id)
            .then(function () {
              ngNotify.set('Tour actualizado', 'success')
              getTours()
            })
            .catch(function () {
              console.error(err);
              ngNotify.set(err,'error')
            })
        }

      }]);
})();
