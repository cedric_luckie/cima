(function () {
  'use strict';
  angular
    .module('app.tour.update',[])
      .directive('tourUpdate',function () {
        return {
            restrict: 'E',
            templateUrl: 'scripts/tour/update.html',
            controller:'UpdateTourCtrl',
            controllerAs:'ctrl'
          };
      })
      .controller('UpdateTourCtrl',['ngNotify','Tour','$routeParams','$scope',function(ngNotify,Tour,$routeParams,$scope){

        var vm = this;

        Tour.show($routeParams.id)
          .success(function (data) {
            vm.tour = data;
          })
          .error(function (err) {
            ngNotify.set(err,'error');
          });

        vm.save = function () {
          Tour.update(vm.tour)
            .success(function (data) {
              ngNotify.set(data,'success');
            })
            .error(function (err) {
              ngNotify.set(err,'error');
            });
        }

        $scope.upload = function(files) {
          console.log('Subiendo archivo');

          vm.loading = true;
          var fd = new FormData();
          //Take the first selected file

          if (files[0]) {
            fd.append("file", files[0]);

            Tour.imagen($routeParams.id,fd)
              .success(function (data) {
                vm.tour.imagen = data;        
                ngNotify.set('Imagen actualizada','success');
              })
              .error(function (err) {
                ngNotify.set(err,'error');
              });
          }
        };


      }]);
})();
