(function () {
  'use strict';
  angular
    .module('app.tour.create',[])
      .directive('createTour',function () {
        return {
            restrict: 'E',
            templateUrl: 'scripts/tour/create.html',
            controller:'CreateTourCtrl',
            controllerAs:'ctrl'
          };
      })
      .controller('CreateTourCtrl',[
        'ngNotify','Tour','$scope','$window',
        function(ngNotify,Tour,$scope,$window){

        var vm = this;
        vm.loading = false;


        $scope.upload = function(files) {
          console.log('Subiendo archivo');

          var nombre = prompt('Escribe el nombre del tour')

          vm.loading = true;
          var fd = new FormData();
          //Take the first selected file

          if (files[0]) {
            fd.append("file", files[0]);

            Tour.upload(fd, nombre)
              .success(function (data) {
                console.log(data)
                $window.location = '/admin/tour/' + data;
                ngNotify.set('Tour creado','success');
                vm.loading = false;
              })
              .error(function (err) {
                ngNotify.set(err,'error');
                vm.loading = false;
              });
          }
        };


      }]);
})();
