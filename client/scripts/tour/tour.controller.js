(function () {
  'use strict';
  angular.module('app.tour', [
    'app.tour.service',
    'app.tour.update',
    'app.tour.list',
    'app.tour.detalle',
    'app.tour.create'
  ]);

})();
