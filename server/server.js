require("babel/register");
var config 	  = require('./config');
var app    	  = require('./config/express').app;
var colors 	  = require('colors');
var express   = require('express')

//Api con la que se comunicará Angularjs y las aplicaciones móviles
app.use('/api/', require('./routes/api'));

//Rutas exclusivas que usaremos en node.js
app.use('/', require('./routes/index'));

app.use('/.well-known/acme-challenge/', express.static(__dirname + '/../docker-nginx/data/certbot/conf'))

// Errores más amigables en caso de cosas inesperado
app.use(function(req, res) {

  // console.error(err);
    // res.status(err.status || 500);
  res.status(404).json('Not found.');
});

app.use(function(err, req, res) {

  console.error(err);
    res.status(err.status || 500).json(err);
  // res.render('error', {
  //     message: 'Ocurrió algo inesperado dentro de la aplicación, estamos trabajando para resolverlo.',
  //     error: {}
  // });
});

app.listen(config.port);
console.log(colors.blue('=========== Iniciando aplicación. Puerto '+config.port+' ============'));
