# DEL BOSQUE
Catálogo de tours virtuales de CIMA

# Instrucciones
1. Buscar el tour en la computadora y comprimirlo, debe quedar en formato .zip
2. Entrar al panel de administración de tours: tours.bosque.com/admin
3. Seleccionar **Nuevo tour**
4. Buscar el archivo comprimido en la computadora
5. Llenar el formulario y presionar **Agregar**
6. Copiar el código que se muestra en la pantalla
7. Entrar al CRM de CIMA y buscar la casa para la cual se esté creando el tour
8. En la descripción de la casa pegar el código
