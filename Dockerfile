
FROM node:6.9.1

COPY package.json package.json
COPY package-lock.json package-lock.json

RUN npm install --production --only=prod


## copiar archivos fuentes al contenedor
# COPY --chown=microservice:microservice . .
COPY . .

# RUN npm run build


## puerto en el que corre 
EXPOSE 12477

# Log debugging variables


## Comando con el que levantas tu aplicación
CMD [ "node",  "server/server.js" ]
